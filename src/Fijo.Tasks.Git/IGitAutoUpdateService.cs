namespace Fijo.Tasks.Git {
	public interface IGitAutoUpdateService {
		void Update(string gitCheckoutPath, string targetBaseFolderInRepository, string repoUrl, string sourceDictionary, string sourceFileExtention, string sourceFilePattern, string commitMessage);
	}
}