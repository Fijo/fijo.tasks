﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using Fijo.VCS.Git.Interfaces;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.FileTools;
using FijoCore.Infrastructure.LightContrib.Module.Path;
using FijoCore.Infrastructure.LightContrib.Repositories;

namespace Fijo.Tasks.Git {
	public class GitAutoUpdateService : IGitAutoUpdateService {
		private readonly AllFileRepository _allFileRepository = Kernel.Resolve<AllFileRepository>();
		private readonly FileFilterService _fileFilterService = Kernel.Resolve<FileFilterService>();
		private readonly IGitService _gitService = Kernel.Resolve<IGitService>();
		private readonly IPathService _pathService = Kernel.Resolve<IPathService>();
		private const string RelativeRepoDir = "_repo";

		public void Update(string gitCheckoutPath, string targetBaseFolderInRepository, string repoUrl, string sourceDictionary, string sourceFileExtention, string sourceFilePattern, string commitMessage) {
			InternalUpdate(gitCheckoutPath, targetBaseFolderInRepository, repoUrl, sourceDictionary, sourceFileExtention, GetRegex(sourceFilePattern), commitMessage);
		}

		private void InternalUpdate(string gitCheckoutPath, string targetBaseFolderInRepository, string repoUrl, string sourceDictionary, string sourceFileExtention, Regex regex, string commitMessage) {
			CreateRepository(CreateWorkingDirectory(gitCheckoutPath), repoUrl);
			var repo = Path.Combine(gitCheckoutPath, RelativeRepoDir);
			var absoluteTargetBase = Path.Combine(repo, targetBaseFolderInRepository);
			CopyAffectedFiles(sourceDictionary, sourceFileExtention, absoluteTargetBase, regex);
			_gitService.Add(repo, _allFileRepository.Get(repo).Select(x => _pathService.GetPathRelativeTo(x, repo)).Where(x => !x.StartsWith(".git")));
			_gitService.Commit(repo, Enumerable.Empty<string>(), commitMessage);
			PushChanges(repo);
		}

		private void PushChanges(string repo) {
			_gitService.Push(repo, "origin", "master");
			Thread.Sleep(30000);
		}

		private void CopyAffectedFiles(string sourceDictionary, string sourceFileExtention, string absoluteTargetBase, Regex regex) {
			var affectedFiles = GetAffectedFiles(sourceDictionary, sourceFileExtention);
			foreach (var file in affectedFiles) PrepareFile(sourceDictionary, absoluteTargetBase, file, regex);
		}

		private void PrepareFile(string sourceFolder, string absoluteTargetBase, string affectedFile, Regex regex) {
			var fileToAdd = _pathService.GetPathRelativeTo(affectedFile, sourceFolder);
			if(UseFile(regex, fileToAdd)) CopyAndMayCreateDirs(sourceFolder, absoluteTargetBase, fileToAdd);
		}

		private bool UseFile(Regex regex, string fileToAdd) {
			return regex.IsMatch(fileToAdd);
		}

		private void CopyAndMayCreateDirs(string sourceFolder, string absoluteTargetBase, string fileToAdd) {
			var fileToCreate = Path.Combine(absoluteTargetBase, fileToAdd);
			_pathService.MayCreateParentFolders(Path.GetDirectoryName(fileToCreate));
			var sourceFile = Path.Combine(sourceFolder, fileToAdd);
			File.Copy(sourceFile, fileToCreate, true);
		}

		private IEnumerable<string> GetAffectedFiles(string sourceFolder, string sourceFileExtention) {
			return _fileFilterService.FilterByExt(_allFileRepository.Get(sourceFolder), sourceFileExtention);
		}

		private string CreateWorkingDirectory(string gitCheckoutPath) {
			if(Directory.Exists(gitCheckoutPath)) Directory.Delete(gitCheckoutPath, true);
			Directory.CreateDirectory(gitCheckoutPath);
			return gitCheckoutPath;
		}

		private void CreateRepository(string repoPath, string repoUrl) {
			_gitService.Clone(repoPath, repoUrl, RelativeRepoDir);
			Thread.Sleep(10000);
		}

		private Regex GetRegex(string pattern) {
			return new Regex(pattern);
		}
	}
}
