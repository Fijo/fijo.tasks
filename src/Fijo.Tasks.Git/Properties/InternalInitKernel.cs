using Fijo.VCS.Git.Helper.InitKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;
using FijoCore.Infrastructure.LightContrib.Helper.InitKernel;

namespace Fijo.Tasks.Git.Properties {
	public class InternalInitKernel : ExtendedInitKernel
	{
		public override void PreInit()
		{
			LoadModules(new LightContribInjectionModule(), new GitInjectionModule(), new GitTaskInjectionModule());
		}
	}
}