﻿using System.Collections.Generic;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Interface;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace Fijo.Tasks.Git.Properties
{
    public class GitTaskInjectionModule : IExtendedNinjectModule {
        public IKernel Kernel { get; private set; }
    	public IDictionary<IExtendedInitHandler, object> ExtendedInit { get; set; }

    	public void AddModule(IKernel kernel) {}

    	public void OnLoad(IKernel kernel)
        {
            Kernel = kernel;

    		kernel.Bind<IGitAutoUpdateService>().To<GitAutoUpdateService>().InSingletonScope();
    	}

    	public void Init(IKernel kernel) {}

    	public void RegisterHandlers(IKernel kernel) {}

    	public void Validate() {}
		
    	public void Loaded(IKernel kernel) {}

        public void OnUnload(IKernel kernel) {}

    	public string Name
        {
            get { return "GitTaskInjectionModule"; }
        }
    }
}