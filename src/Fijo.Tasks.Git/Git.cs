using Fijo.Tasks.Git.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using NAnt.Core;
using NAnt.Core.Attributes;

namespace Fijo.Tasks.Git {
	[TaskName("Git")]
	public class Git : Task {
		private readonly IGitAutoUpdateService _gitAutoUpdateService;

		public Git() {
			new InternalInitKernel().Init();
			_gitAutoUpdateService = Kernel.Resolve<IGitAutoUpdateService>();
		}

		[TaskAttribute("SourceFilesWithExtention", Required = true)]
		protected string SourceFilesWithExtention { get; set; }

		[TaskAttribute("SourceFilePattern", Required = true)]
		protected string SourceFilePattern { get; set; }
		
		[TaskAttribute("CommitMessage", Required = true)]
		protected string CommitMessage { get; set; }
		
		[TaskAttribute("SourceDictionary", Required = true)]
		protected string SourceDictionary { get; set; }
		
		[TaskAttribute("RepoUrl", Required = true)]
		protected string RepoUrl { get; set; }
		
		[TaskAttribute("UpdateRepoBaseFoder", Required = true)]
		protected string UpdateRepoBaseFoder { get; set; }
		
		[TaskAttribute("CheckoutPath", Required = true)]
		protected string CheckoutPath { get; set; }

		protected override void ExecuteTask() {
			_gitAutoUpdateService.Update(CheckoutPath, UpdateRepoBaseFoder, RepoUrl, SourceDictionary, SourceFilesWithExtention, SourceFilePattern, CommitMessage);
		}
	}
}