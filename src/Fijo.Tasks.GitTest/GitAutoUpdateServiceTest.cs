using System;
using System.IO;
using Fijo.Tasks.Git;
using Fijo.Tasks.Git.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;
using NUnit.Framework;

namespace Fijo.Tasks.GitTest {
	[TestFixture]
	public class GitAutoUpdateServiceTest {
		private IGitAutoUpdateService _gitAutoUpdateService;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_gitAutoUpdateService = new GitAutoUpdateService();
		}

		[Test]
		public void UpdateTest() {
			var tmpTestFilePath = Path.Combine("Resources", "TestFiles");
			var gitCheckoutPath = Path.Combine(Environment.CurrentDirectory, "gitCheckout" + Guid.NewGuid().ToString());
			_gitAutoUpdateService.Update(gitCheckoutPath, "bin",
			                             Kernel.Resolve<IConfigurationService>().Get<string>("Fijo.Tasks.GitTest.GitAutoUpdateServiceTest.TestRepository"), tmpTestFilePath, "dll", ".*?",
			                             string.Format("test task commit{0}", Guid.NewGuid().ToString()));
			Assert.True(Directory.Exists(gitCheckoutPath));
		}
	}
}